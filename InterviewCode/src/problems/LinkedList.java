package problems;

/**
 * 
 * @author Lakshay Ghai
 *
 */
public class LinkedList {
 
	Node head;
    /* Linked list Node*/
    class Node 
    {
        int data;
        Node next;
 
        Node(int d) 
        {
            data = d;
            next = null;
        }
    }
    
    public  boolean isPalindrome(Node head) {
    	Node slowPtr = head; 
    	Node fastPtr = head; 
        boolean result = true;
        if (head != null && head.next != null) {
        	 while(fastPtr.next!=null && fastPtr.next.next!=null){ 
                fastPtr = fastPtr.next.next;
                slowPtr = slowPtr.next;
            }
        	Node secondHalf = slowPtr.next;
        	slowPtr.next = null;
        	Node secondList = reverse(secondHalf);
        	result = compareLists(head, secondList);
        }
        return result;
    }
        	 
    private  boolean compareLists(Node head1, Node head2) {
        Node temp1 = head1;
        Node temp2 = head2;
        while (temp1 != null && temp2 != null) {
            if (temp1.data != temp2.data) {
        	   return false;
            }
            temp1 = temp1.next;
    		temp2 = temp2.next;
        }
        return true;
    }
    
    private  Node reverse(Node secondList) {
        Node prev = null;
        Node current = secondList;
        Node next;
        while (current != null) {
            next = current.next;
    	    current.next = prev;
    	    prev = current;
    	    current = next;
    	}
        secondList = prev;
        return secondList;
    }
    
    public void push(int newData) {
        Node newNode = new Node(newData);
        newNode.next = head;
        head = newNode;
    }
    
    public static void main(String[] args) {
        
        LinkedList llist = new LinkedList();
 
        llist.push(2);
        llist.push(3);
        llist.push(1);
        llist.push(3);
        llist.push(2);
        if (llist.isPalindrome(llist.head) == true) {
            System.out.println("Is Palindrome");
            System.out.println("");
        }
        else {
            System.out.println("Not Palindrome");
            System.out.println("");
        }
    }
}
        		     
        	 
