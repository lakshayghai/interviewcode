package problems;
/**
Combination Sum Problem
Problem:
Given a collection of candidate numbers (C) and a target number (T), find all unique
combinations in C where the candidate numbers sums to T. Each number in C may only be
used once in the combination.
Note:
All numbers (including target) will be positive integers.
Elements in a combination (a1, a2,..., ak) must be in non-descending order.(ie, a1 <= a2 <=...<= ak).

The solution set must not contain duplicate combinations.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author Lakshay Ghai
 *
 */

class CombinationSumProblem {
	
    public static Set<List<Integer>> combinationSum(int[] candidates, int target) {
        Set<List<Integer>> result = new HashSet<List<Integer>>();

        if(candidates == null || candidates.length == 0) return result;
     
        List<Integer> current = new ArrayList<Integer>();
        Arrays.sort(candidates);
     
        combinationSum(candidates, target, 0, current, result);
     
        return result;
    }
     
    private static void combinationSum(int[] candidates, int target, int j, List<Integer> curr, Set<List<Integer>> result){
       
        if(target < 0){
            return;
        }
        if(target == 0){
            List<Integer> temp = new ArrayList<Integer>(curr);
            result.add(temp);
            return;
        }
     
        for(int i=j; i<candidates.length; i++){
            if(target < candidates[i]){
            	return;	
            }
                
            curr.add(candidates[i]);
            combinationSum(candidates, target - candidates[i], i+1, curr, result);
            curr.remove(curr.size()-1); 
        }
    }
    
    public static void main(String args[]) {
        /*int[] numbers = {3,9,8,4,5,7,10};
    	int target = 15;*/
        int[] numbers = {10,1,2,7,6,1,5};
        int target = 8;
        System.out.println(combinationSum(numbers,target));
    }
}
